﻿using System;
using System.IO;

namespace MLP.Data
{
    public static class DataReaderFactory
    {
        public static IDataReader GetReader(string filename)
        {
            string extension = Path.GetExtension(filename).ToLower();
            switch (extension)
            {
                case ".xlsx":
                    return new ExcelDataReader();

                case ".csv":
                    return new CsvDataReader();

                default:
                    throw new NotImplementedException(extension);
            }
        }
    }
}