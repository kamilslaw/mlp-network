﻿namespace MLP.Data
{
    public class RawDataContext
    {
        public RawDataContext()
        {
            ClassColumnIndex = -1;
        }

        public object[][] Data { get; set; }
        public int ClassColumnIndex { get; set; }
        public int[] ExcludedColumnsIndexes { get; set; }
        public int[] ExcludedRowsIndexes { get; set; }

        public int RowsCount { get { return Data.Length - ExcludedRowsIndexes.Length; } }
    }
}
