﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MLP.Desktop.Forms
{
    public partial class LoadingForm : Form, IProgress<int>
    {
        public LoadingForm() : this(null)
        {
        }

        public LoadingForm(Action<IProgress<int>> actionToPerform)
        {
            InitializeComponent();

            if (!DesignMode)
            {
                Task.Factory.StartNew(() => actionToPerform(this), TaskCreationOptions.LongRunning);
            }
        }

        public void Report(int value)
        {
            Invoke(new Action(delegate ()
            {
                progressBar.Value = Math.Max(value, 1);
                if (value == 100)
                {
                    Close();
                }
            }));
        }

        #region Move Window

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        #endregion
    }
}
