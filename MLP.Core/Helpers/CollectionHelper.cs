﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MLP.Core.Helpers
{
    internal static class CollectionHelper
    {
        private static readonly Random _random = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = _random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        // Normalize to range [0;1]. Max & min value are taken from columns, not rows.
        public static void Normalize(this IList<List<double>> vectorsList)
        {
            var columnsCount = vectorsList[0].Count;
            for (int i = 0; i < columnsCount; i++)
            {
                double min = vectorsList.Min(r => r[i]);
                double max = vectorsList.Max(r => r[i]);
                foreach (var list in vectorsList)
                {
                    list[i] = max == min ? 0.5 : (list[i] - min) / (max - min);
                }
            }
        }

        // https://stackoverflow.com/questions/28867327/looping-through-a-n-dimensional-array
        public static void ForEachNode<T>(this Array array, Action<T, int[]> action)
        {
            int i = 0;
            foreach (var element in array)
            {
                var coordinates = IndexToCoordinates(i++, array);
                action((T)element, coordinates);
            }
        }

        public static List<TOut> SelectEachNode<TIn, TOut>(this Array array, Func<TIn, int[], TOut> func)
        {
            List<TOut> result = new List<TOut>();
            int i = 0;
            foreach (var element in array)
            {
                var coordinates = IndexToCoordinates(i++, array);
                result.Add(func((TIn)element, coordinates));
            }

            return result;
        }

        #region Private Methods

        private static int[] IndexToCoordinates(int i, Array array)
        {
            var dimensions = Enumerable.Range(0, array.Rank).Select(array.GetLength).ToArray();
            Func<int, int, int> product = (i1, i2) => i1 * i2;
            return dimensions.Select((d, n) => (i / dimensions.Take(n).Aggregate(1, product)) % d).ToArray();
        }

        #endregion
    }
}
