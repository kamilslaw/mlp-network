﻿using MLP.Core;
using MLP.Core.Models;
using MLP.Data;
using MLP.Desktop.Forms;
using MLP.Desktop.Utils;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MLP.Desktop
{
    public partial class ConstructForm : Form
    {
        public ConstructForm()
        {
            InitializeComponent();
        }

        private void buttonSelectData_Click(object sender, EventArgs e)
        {
            // Open file with data
            var filename = OpenFileHelper.GeDataFilenameFromDialog();
            if (string.IsNullOrEmpty(filename)) return;
            var rawData = DataReaderFactory.GetReader(filename).ReadRawData(filename);
            // Show data dialog
            using (SelectDataForm selectDataForm = new SelectDataForm(rawData, forceClassColumnSelection: true))
            {
                if (selectDataForm.ShowDialog() == DialogResult.Yes)
                {
                    // Create network
                    var networkData = new NetworkData(selectDataForm.RawDataContext);
                    var network = new NeuralNetwork(networkData, GenerateNetworkModelFromView());
                    // Start training
                    TrainingLog trainingResult = null;
                    new LoadingForm((progress) => trainingResult = network.Train(progress)).ShowDialog();
                    // Show effects
                    new TrainedNetworkForm(trainingResult).ShowDialog();
                }
            }
        }

        private void checkBoxUseSOM_CheckedChanged(object sender, EventArgs e)
        {
            textBoxSOMDimensions.Enabled = textBoxSOMEpochs.Enabled = comboBoxSOMGrid.Enabled = checkBoxUseSOM.Checked;
            textBoxSOMDimensions.BackColor = textBoxSOMEpochs.BackColor = comboBoxSOMGrid.BackColor = checkBoxUseSOM.Checked ? Color.White : Color.LightGray;
        }

        private NetworkSettings GenerateNetworkModelFromView()
        {
            bool useSOM = checkBoxUseSOM.Checked;
            return new NetworkSettings()
            {
                Layers = textBoxLayers.Text.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray(),
                SubLayersCount = (int)numericUpDownSublayers.Value,
                Epochs = int.Parse(textBoxEpochs.Text),
                UseBias = checkBoxBias.Checked,
                PercentToTest = (int)numericUpDownTestPercent.Value,

                UseSom = useSOM,
                SOMDimensions = !useSOM ? null : textBoxSOMDimensions.Text.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray(),
                SOMEpochs = !useSOM ? -1 : int.Parse(textBoxSOMEpochs.Text),
                SOMGrid = comboBoxSOMGrid.Text == "Rectangular" ? SOMGrid.Rectangular : SOMGrid.Hexagonal
            };
        }
    }
}
