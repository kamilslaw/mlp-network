﻿using MLP.Core.Helpers;
using MLP.Core.Models;
using MLP.Data;
using System;
using System.Collections.Generic;
using System.Collections;

namespace MLP.Core.Construction
{
    internal class SOMStructure : BaseStructure, IEnumerable<SOMNode>
    {
        private Array _nodes;

        public SOMStructure(NetworkData data, NetworkSettings settings) : base(data, settings)
        {
            _nodes = Array.CreateInstance(typeof(SOMNode), settings.SOMDimensions);
            _nodes.ForEachNode<SOMNode>((_, coordinates) => _nodes.SetValue(new SOMNode(data.InputsCount, coordinates), coordinates));
        }

        #region Public Methods

        public void Train(int cycle)
        {
            for (int d = 0; d < _data.Values.Length; d++)
            {
                int[] closestNodeIndex = GetClosestNode(_data.Values[d]);                
                _nodes.ForEachNode<SOMNode>((node, coordinates) =>
                {
                    node.Upate(cycle, closestNodeIndex, _data.Values[d], _settings.SOMGrid == SOMGrid.Hexagonal); // Update distances
                });
            }
        }

        public SOMColoring GenarateColors()
        {
            var result = new SOMColoring(_settings.SOMDimensions);

            for (int d = 0; d < _data.Values.Length; d++)
            {
                int[] closestNodeIndex = GetClosestNode(_data.Values[d]);
                result.AddElement(_data.Values[d].LabelIndex, closestNodeIndex);
            }

            return result;
        }

        #endregion

        #region IEnumerable

        public IEnumerator<SOMNode> GetEnumerator() => new Enumerator(_nodes);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private class Enumerator : IEnumerator<SOMNode>
        {
            private IEnumerator enumerator;

            public Enumerator(Array nodes)
            {
                enumerator = nodes.GetEnumerator();
            }

            public SOMNode Current => (SOMNode)enumerator.Current;

            object IEnumerator.Current => Current;

            public void Dispose() { }

            public bool MoveNext() => enumerator.MoveNext();

            public void Reset() => enumerator.Reset();
        }

        #endregion

        #region Private Methods

        private int[] GetClosestNode(DataRow row)
        {
            double min = double.MaxValue;
            int[] referenceIndex = null;
            _nodes.ForEachNode<SOMNode>((node, coordinates) =>
            {
                var distance = node.GetDistance(row);
                if (min > distance)
                {
                    min = distance;
                    referenceIndex = coordinates;
                }
            });

            return referenceIndex;
        }

        #endregion
    }
}
