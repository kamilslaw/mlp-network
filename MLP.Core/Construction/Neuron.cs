﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MLP.Core.Construction
{
    internal class Neuron : INode
    {
        private const double BETA = 1;
        private const double ETA = 0.9;

        public double S { get; set; }
        public double Y { get; set; } // input value (X) for input neurons
        public double Error { get; set; }

        public List<Synapse> PreviousSynapses { get; set; }
        public List<Synapse> NextSynapses { get; set; }

        public Neuron()
        {
            PreviousSynapses = new List<Synapse>();
            NextSynapses = new List<Synapse>();
        }

        public void UpdateValues()
        {
            S = PreviousSynapses.Sum(s => s.W * s.Previous.Y);
            Y = 1 / (1 + Math.Exp(-BETA * S));
        }

        public double UpdateError(bool isActive)
        {
            var desiredValue = isActive ? 1 : 0;
            return Error = desiredValue - Y;
        }

        public void PropagateError()
        {
            foreach (var synapse in NextSynapses)
            {
                var delta = ETA * synapse.Next.Error * (1 - synapse.Next.Y) * synapse.Next.Y * Y;
                synapse.W += delta;
            }

            Error = NextSynapses.Sum(s => s.Next.Error * s.W * (1 - s.Next.Y) * s.Next.Y);
        }
    }
}
