﻿using SpreadsheetLight;
using System.IO;
using System.Linq;

namespace MLP.Data
{
    public class ExcelDataReader : IDataReader
    {
        public object[][] ReadRawData(string filename)
        {
            using (var fileStream = new FileStream(filename, FileMode.Open))
            using (SLDocument sheet = new SLDocument(fileStream))
            {
                SLWorksheetStatistics stats = sheet.GetWorksheetStatistics();

                int columnsCount = 0;
                while (!string.IsNullOrEmpty(sheet.GetCellValueAsString(stats.StartRowIndex, stats.StartColumnIndex + columnsCount))) columnsCount++;

                return Enumerable.Range(stats.StartRowIndex, stats.NumberOfRows)
                                 .Select(r => Enumerable.Range(stats.StartColumnIndex, columnsCount).Select(c => sheet.GetCellValueAsString(r, c)).ToArray())
                                 .ToArray();
            }
        }
    }
}