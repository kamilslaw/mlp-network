﻿using System.Collections.Generic;

namespace MLP.Core.Construction
{
    internal interface INode
    {
        double Error { get; }
        double Y { get; }
        List<Synapse> PreviousSynapses { get; }
        List<Synapse> NextSynapses { get; }
        void UpdateValues();
        void PropagateError();
    }
}
