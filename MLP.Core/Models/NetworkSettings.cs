﻿namespace MLP.Core.Models
{
    public class NetworkSettings
    {
        public int[] Layers { get; set; }
        public int SubLayersCount { get; set; }
        public int Epochs { get; set; }
        public bool UseBias { get; set; }
        public int PercentToTest { get; set; }

        // SOM
        public bool UseSom { get; set; }
        public int[] SOMDimensions { get; set; }
        public int SOMEpochs { get; set; }
        public SOMGrid SOMGrid { get; set; }

        public int EpochsTotal { get { return Epochs * SubLayersCount + (UseSom ? SOMEpochs : 0); } }
        public bool ExtractToTest { get { return PercentToTest > 0; } }
    }

    public enum SOMGrid
    {
        Rectangular,
        Hexagonal
    }
}
