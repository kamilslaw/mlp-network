﻿using System;
using MLP.Core.Models;
using MLP.Data;
using System.Collections.Generic;
using System.Linq;
using MLP.Core.Helpers;

namespace MLP.Core.Construction
{
    internal class NetworkStructure : BaseStructure
    {
        private List<List<Neuron>> _network;
        private SOMStructure _som;

        public NetworkStructure(NetworkData data, NetworkSettings settings, SOMStructure som) : base(data, settings)
        {
            _som = som;
            Construct();
        }

        public double Train()
        {
            double error = 0;
            foreach (var row in _data.Values)
            {
                // Set input neurons
                for (int i = 0; i < row.Count; i++)
                    _network[0][i].Y = row[i];
                // Compute values (skip input layer)
                if (_settings.UseSom)
                    foreach (var node in _som)
                        node.UpdateValues();
                foreach (var layer in _network.Skip(1))
                    foreach (var neuron in layer)
                        neuron.UpdateValues();
                // Update error value for output neurons
                var outputLayer = _network.Last();
                for (int i = 0; i < outputLayer.Count; i++)
                    error += Math.Pow(outputLayer[i].UpdateError(i == row.LabelIndex), 2);
                // Compute new synapses values (skip output layer)
                for (int i = _network.Count - 2; i >= 1; i--)
                    foreach (var neuron in _network[i])
                        neuron.PropagateError();
                if (_settings.UseSom)
                    foreach (var node in _som)
                        node.PropagateError();
                foreach (var neuron in _network[0])
                    neuron.PropagateError();
            }

            return error;
        }

        public CheckingLog Check(DataRow[] values)
        {
            var normalizedValues = values.Select(d => d.Clone()).ToArray();
            normalizedValues.Normalize();
            CheckingLog result = new CheckingLog();
            for (int d = 0; d < values.Length; d++)
            {
                // Set input neurons
                for (int i = 0; i < normalizedValues[d].Count; i++)
                    _network[0][i].Y = normalizedValues[d][i];
                // Compute values (skip input layer)
                if (_settings.UseSom)
                    foreach (var node in _som)
                        node.UpdateValues();
                foreach (var layer in _network.Skip(1))
                    foreach (var neuron in layer)
                        neuron.UpdateValues();
                // Add to result
                var outputLayer = _network.Last();
                var outputLayerValues = outputLayer.Select(n => n.Y).ToArray();
                var labelIndex = GetMaxIndex(outputLayer, n => n.Y);
                var label = _data.Labels[labelIndex];
                var modelContainsResult = values[d].LabelIndex >= 0;
                var error = !modelContainsResult ? 0 : outputLayerValues.Select((v, i) => i == values[d].LabelIndex ? 1 - v : v).Sum();
                var isCorrect = !modelContainsResult ? (bool?)null : labelIndex == values[d].LabelIndex;
                result.AddPosition(values[d], label, outputLayerValues, error, isCorrect);
            }

            return result;
        }

        public void AddSublayer(int subLayerNo)
        {
            var oldOutputLayers = _network.Where((_, i) => i > 0 && i % (1 + _settings.Layers.Length) == 0).ToList();
            var firstNewSublayerIndex = _network.Count;
            // Create neurons
            for (int i = 0; i < _settings.Layers.Length; i++)
                _network.Add(
                    Enumerable.Range(0, _settings.Layers[i]).Select(_ => new Neuron()).ToList());
            _network.Add(
                Enumerable.Range(0, _data.OutputsCount).Select(_ => new Neuron()).ToList());
            // Create synapses between previous outputs & new sublayer first layer
            foreach (var outputLayer in oldOutputLayers)
                foreach (var neuron in outputLayer)
                    foreach(var newNeuron in _network[firstNewSublayerIndex])
                        new Synapse(neuron, newNeuron, GenerateSynapseValue());
            // Create synapses between input & new sublayer first layer
            foreach (var inputNeuron in _network.First())
                foreach (var newNeuron in _network[firstNewSublayerIndex])
                    new Synapse(inputNeuron, newNeuron, GenerateSynapseValue());
            // Create synapses between every neuron across layers
            for (int i = 1 + (subLayerNo * _settings.Layers.Length + 1); i < _network.Count - 1; i++)
                for (int j = 0; j < _network[i].Count; j++)
                    for (int k = 0; k < _network[i + 1].Count; k++)
                        new Synapse(_network[i][j], _network[i + 1][k], GenerateSynapseValue());
            // Bias
            if (_settings.UseBias)
                foreach (var layer in _network.Skip(firstNewSublayerIndex))
                    foreach (var neuron in layer)
                        new Synapse(new Neuron() { Y = 1 }, neuron, GenerateSynapseValue());
        }

        private void Construct() // TODO: add multilayers
        {
            _network = new List<List<Neuron>>();
            // Create neurons
            _network.Add(
                Enumerable.Range(0, _data.InputsCount).Select(_ => new Neuron()).ToList());
            for (int i = 0; i < _settings.Layers.Length; i++)
                _network.Add(
                    Enumerable.Range(0, _settings.Layers[i]).Select(_ => new Neuron()).ToList());
            _network.Add(
                Enumerable.Range(0, _data.OutputsCount).Select(_ => new Neuron()).ToList());
            // Create synapses between every neuron across layers
            for (int i = 0; i < _network.Count - 1; i++)
                for (int j = 0; j < _network[i].Count; j++)
                    for (int k = 0; k < _network[i + 1].Count; k++)
                        new Synapse(_network[i][j], _network[i + 1][k], GenerateSynapseValue());
            // Create synapses between network & SOM
            if (_settings.UseSom)
            {                
                foreach (var node in _som)
                {
                    for (int i = 0; i < _network[0].Count; i++)
                        new Synapse(_network[0][i], node, GenerateSynapseValue());
                    for (int i = 0; i < _network[1].Count; i++)
                        new Synapse(node, _network[1][i], GenerateSynapseValue());
                }
            }
            // Bias
            if (_settings.UseBias)
                foreach (var layer in _network.Skip(1))
                    foreach (var neuron in layer)
                        new Synapse(new Neuron() { Y = 1 }, neuron, GenerateSynapseValue());
        }

        private int GetMaxIndex(List<Neuron> layer, Func<Neuron, double> getValueFunc)
        {
            int index = 0;
            double value = double.MinValue;
            for (int i = 0; i < layer.Count; i++)
            {
                var tmpValue = getValueFunc(layer[i]);
                if (tmpValue > value)
                {
                    index = i;
                    value = tmpValue;

                }
            }

            return index;
        }

        private static double GenerateSynapseValue() => _random.NextDouble() * 0.2 - 0.1;
    }
}
