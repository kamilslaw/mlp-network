﻿using MLP.Data;
using System;
using System.Linq;
using System.Collections.Generic;

namespace MLP.Core.Construction
{
    internal class SOMNode : INode
    {
        private const double ETA = -0.9;

        protected static readonly Random _random = new Random();

        private static int y0 = 1, alfa = 1000;
        private static double sigma0 = 3;

        private double[] _values;
        private int[] _coordinates;

        public SOMNode(int length, int[] coordinates)
        {
            _coordinates = coordinates;
            _values = Enumerable.Range(1, length).Select(_ => 1.0 / _random.Next(10, 101)).ToArray();
            PreviousSynapses = new List<Synapse>();
            NextSynapses = new List<Synapse>();
        }

        #region INode

        public double Error => 0;
        public double Y { get; set; }
        public List<Synapse> PreviousSynapses { get; set; }
        public List<Synapse> NextSynapses { get; set; }

        // We use the distance between node & input data row
        public void UpdateValues()
        {
            Y = GetDistance(PreviousSynapses.Select(s => s.Previous.Y).ToList());
        }

        public void PropagateError()
        {
            foreach (var synapse in NextSynapses)
            {
                var delta = -ETA * synapse.Next.Error * (1 - synapse.Next.Y) * synapse.Next.Y * Y;
                synapse.W += delta;
            }
        }

        #endregion

        public double GetDistance(List<double> input)
        {
            return Math.Sqrt(_values.Select((el, i) => Math.Pow(input[i] - el, 2)).Sum());
        }

        public void Upate(int cycle, int[] referenceIndex, DataRow row, bool hexagonal)
        {
            var sigma = sigma0 * Math.Exp(-cycle / alfa);
            var distance = _coordinates.Select((c, i) => hexagonal ? Math.Pow(c - referenceIndex[i], 2) : Math.Abs(c - referenceIndex[i])).Sum();
            var delta = Math.Exp(-Math.Pow(distance, 2) / (2 * sigma * sigma));
            var y = y0 * Math.Exp(-cycle / alfa);
            for (int i = 0; i < _values.Length; i++)
                _values[i] += delta * y * (row[i] - _values[i]);
        }
    }
}
