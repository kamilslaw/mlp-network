﻿using MLP.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AGDS.Desktop
{
    public class AGDS
    {
        public NetworkData Data { get; }
        public AGDSRow[] Rows { get; }
        public AGDSNode[][] Nodes { get; }

        public AGDS(NetworkData data)
        {
            Data = data;

            double[][] values = new double[data.InputsCount][];
            for (int i = 0; i < data.InputsCount; i++)
                values[i] = new double[data.Values.Length];
            for (int i = 0; i < data.InputsCount; i++)
                for (int j = 0; j < data.Values.Length; j++)
                    values[i][j] = data.Values[j][i];
            double[][] distinctedValues = values.Select(x => x.Distinct().OrderBy(y => y).ToArray()).ToArray();
            Nodes = distinctedValues.Select(x => x.Select(y => new AGDSNode(y)).ToArray()).ToArray();
            foreach (var row in Nodes)
                for (int i = 0; i < row.Length - 1; i++)
                    row[i].RightWeight = (row[i + 1].Value - row[i].Value) / (row[row.Length - 1].Value - row[0].Value);

            Rows = data.Values.Select((x, i) => new AGDSRow(i, x.LabelIndex, x.Select((y, j) => Array.FindIndex(Nodes[j], n => n.Value == y)).ToArray())).ToArray();
        }

        public IEnumerable<object[]> GetDataRows()
        {
            return Rows.Select(r => r.ToDataRow(Data.Labels, Nodes).ToArray());
        }

        public int Classify(AGDSRow row)
        {
            return GetSimilarity(row).ToList()[1].row.LabelIndex;
        }

        public IEnumerable<(AGDSRow row, double factor)> GetSimilarity(AGDSRow row)
        {
            var result = Rows.Select(r => (row: r, factor: GetFactor(row, r))).OrderByDescending(x => x.factor);
            return result;
        }

        private double GetFactor(AGDSRow row, AGDSRow r)
        {
            double multiplier = 1.0 / Data.InputsCount;
            //double multiplier = 1.0 / (Data.InputsCount + 1); // UNCOMMENT TO USE CLASS IN SIMILARITY CHECKING
            var result = Enumerable.Range(0, Data.InputsCount).Sum(i => multiplier * getDistance(Nodes[i], row.ValuesIndexes[i], r.ValuesIndexes[i]));
            // result += multiplier * (row.LabelIndex == r.LabelIndex ? 1 : 0); // UNCOMMENT TO USE CLASS IN SIMILARITY CHECKING
            return result;

            double getDistance(AGDSNode[] nodes, int lIndex, int rIndex)
            {
                if (lIndex == rIndex) return 1;
                if (lIndex > rIndex)
                {
                    int tmp = lIndex; lIndex = rIndex; rIndex = tmp;
                }

                return 1 - Enumerable.Range(lIndex, rIndex - lIndex).Sum(i => nodes[i].RightWeight);
            }
        }
    }

    public struct AGDSNode
    {
        public AGDSNode(double value)
        {
            Value = value;
            RightWeight = -1;
        }

        public double Value { get; }
        public double RightWeight { get; set; }
    }

    public struct AGDSRow
    {
        public AGDSRow(int index, int labelIndex, int[] valuesIndexes) : this()
        {
            Index = index;
            LabelIndex = labelIndex;
            ValuesIndexes = valuesIndexes;
        }

        public int Index { get; }
        public int LabelIndex { get; }
        public int[] ValuesIndexes { get; }

        public string Name { get => $"R{Index:D3}"; }

        public IEnumerable<object> ToDataRow(string[] labels, AGDSNode[][] nodes)
        {
            var values = ValuesIndexes.Select((x, i) => nodes[i][x].Value.ToString("N2")).ToList();
            values.Insert(0, Name);
            values.Add(labels[LabelIndex]);
            return values;
        }
    }
}
