﻿namespace MLP.Desktop
{
    partial class ConstructForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConstructForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonSelectData = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownTestPercent = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBoxBias = new System.Windows.Forms.CheckBox();
            this.textBoxEpochs = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDownSublayers = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxLayers = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxSOMGrid = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxSOMEpochs = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxSOMDimensions = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxUseSOM = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSublayers)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(51)))));
            this.panel1.Controls.Add(this.buttonSelectData);
            this.panel1.Location = new System.Drawing.Point(0, 247);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(403, 59);
            this.panel1.TabIndex = 13;
            // 
            // buttonSelectData
            // 
            this.buttonSelectData.BackColor = System.Drawing.SystemColors.Control;
            this.buttonSelectData.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSelectData.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.buttonSelectData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.buttonSelectData.Image = ((System.Drawing.Image)(resources.GetObject("buttonSelectData.Image")));
            this.buttonSelectData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonSelectData.Location = new System.Drawing.Point(266, 14);
            this.buttonSelectData.Name = "buttonSelectData";
            this.buttonSelectData.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.buttonSelectData.Size = new System.Drawing.Size(118, 30);
            this.buttonSelectData.TabIndex = 36;
            this.buttonSelectData.Text = "Select data";
            this.buttonSelectData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSelectData.UseVisualStyleBackColor = false;
            this.buttonSelectData.Click += new System.EventHandler(this.buttonSelectData_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.numericUpDownTestPercent);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.checkBoxBias);
            this.groupBox5.Controls.Add(this.textBoxEpochs);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.numericUpDownSublayers);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.textBoxLayers);
            this.groupBox5.Controls.Add(this.label1);
            this.groupBox5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.groupBox5.Location = new System.Drawing.Point(12, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(183, 226);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Network";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(158, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 21);
            this.label8.TabIndex = 22;
            this.label8.Text = "%";
            // 
            // numericUpDownTestPercent
            // 
            this.numericUpDownTestPercent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.numericUpDownTestPercent.Location = new System.Drawing.Point(101, 183);
            this.numericUpDownTestPercent.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericUpDownTestPercent.Name = "numericUpDownTestPercent";
            this.numericUpDownTestPercent.Size = new System.Drawing.Size(51, 29);
            this.numericUpDownTestPercent.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 21);
            this.label7.TabIndex = 20;
            this.label7.Text = "Use to test";
            // 
            // checkBoxBias
            // 
            this.checkBoxBias.AutoSize = true;
            this.checkBoxBias.Location = new System.Drawing.Point(13, 150);
            this.checkBoxBias.Name = "checkBoxBias";
            this.checkBoxBias.Size = new System.Drawing.Size(87, 25);
            this.checkBoxBias.TabIndex = 19;
            this.checkBoxBias.Text = "Use Bias";
            this.checkBoxBias.UseVisualStyleBackColor = true;
            // 
            // textBoxEpochs
            // 
            this.textBoxEpochs.BackColor = System.Drawing.Color.White;
            this.textBoxEpochs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.textBoxEpochs.Location = new System.Drawing.Point(101, 108);
            this.textBoxEpochs.MaxLength = 8;
            this.textBoxEpochs.Name = "textBoxEpochs";
            this.textBoxEpochs.Size = new System.Drawing.Size(69, 29);
            this.textBoxEpochs.TabIndex = 18;
            this.textBoxEpochs.Text = "1000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 21);
            this.label3.TabIndex = 17;
            this.label3.Text = "Epochs";
            // 
            // numericUpDownSublayers
            // 
            this.numericUpDownSublayers.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.numericUpDownSublayers.Location = new System.Drawing.Point(101, 68);
            this.numericUpDownSublayers.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSublayers.Name = "numericUpDownSublayers";
            this.numericUpDownSublayers.Size = new System.Drawing.Size(69, 29);
            this.numericUpDownSublayers.TabIndex = 16;
            this.numericUpDownSublayers.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 21);
            this.label2.TabIndex = 11;
            this.label2.Text = "Sublayers";
            // 
            // textBoxLayers
            // 
            this.textBoxLayers.BackColor = System.Drawing.Color.White;
            this.textBoxLayers.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.textBoxLayers.Location = new System.Drawing.Point(101, 28);
            this.textBoxLayers.MaxLength = 100;
            this.textBoxLayers.Name = "textBoxLayers";
            this.textBoxLayers.Size = new System.Drawing.Size(69, 29);
            this.textBoxLayers.TabIndex = 10;
            this.textBoxLayers.Text = "5;";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 21);
            this.label1.TabIndex = 9;
            this.label1.Text = "Layers";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxSOMGrid);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxSOMEpochs);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxSOMDimensions);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.checkBoxUseSOM);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.groupBox1.Location = new System.Drawing.Point(207, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(183, 226);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Self-Organizing Maps";
            // 
            // comboBoxSOMGrid
            // 
            this.comboBoxSOMGrid.Enabled = false;
            this.comboBoxSOMGrid.FormattingEnabled = true;
            this.comboBoxSOMGrid.Items.AddRange(new object[] {
            "Rectangular",
            "Hexagonal"});
            this.comboBoxSOMGrid.Location = new System.Drawing.Point(59, 148);
            this.comboBoxSOMGrid.Name = "comboBoxSOMGrid";
            this.comboBoxSOMGrid.Size = new System.Drawing.Size(113, 29);
            this.comboBoxSOMGrid.TabIndex = 26;
            this.comboBoxSOMGrid.Text = "Rectangular";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 21);
            this.label5.TabIndex = 25;
            this.label5.Text = "Grid";
            // 
            // textBoxSOMEpochs
            // 
            this.textBoxSOMEpochs.BackColor = System.Drawing.Color.LightGray;
            this.textBoxSOMEpochs.Enabled = false;
            this.textBoxSOMEpochs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.textBoxSOMEpochs.Location = new System.Drawing.Point(101, 108);
            this.textBoxSOMEpochs.MaxLength = 8;
            this.textBoxSOMEpochs.Name = "textBoxSOMEpochs";
            this.textBoxSOMEpochs.Size = new System.Drawing.Size(69, 29);
            this.textBoxSOMEpochs.TabIndex = 24;
            this.textBoxSOMEpochs.Text = "1000";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 21);
            this.label6.TabIndex = 23;
            this.label6.Text = "Epochs";
            // 
            // textBoxSOMDimensions
            // 
            this.textBoxSOMDimensions.BackColor = System.Drawing.Color.LightGray;
            this.textBoxSOMDimensions.Enabled = false;
            this.textBoxSOMDimensions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.textBoxSOMDimensions.Location = new System.Drawing.Point(101, 68);
            this.textBoxSOMDimensions.MaxLength = 100;
            this.textBoxSOMDimensions.Name = "textBoxSOMDimensions";
            this.textBoxSOMDimensions.Size = new System.Drawing.Size(69, 29);
            this.textBoxSOMDimensions.TabIndex = 20;
            this.textBoxSOMDimensions.Text = "5;4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 21);
            this.label4.TabIndex = 19;
            this.label4.Text = "Dimensions";
            // 
            // checkBoxUseSOM
            // 
            this.checkBoxUseSOM.AutoSize = true;
            this.checkBoxUseSOM.Location = new System.Drawing.Point(10, 30);
            this.checkBoxUseSOM.Name = "checkBoxUseSOM";
            this.checkBoxUseSOM.Size = new System.Drawing.Size(94, 25);
            this.checkBoxUseSOM.TabIndex = 7;
            this.checkBoxUseSOM.Text = "Use SOM";
            this.checkBoxUseSOM.UseVisualStyleBackColor = true;
            this.checkBoxUseSOM.CheckedChanged += new System.EventHandler(this.checkBoxUseSOM_CheckedChanged);
            // 
            // ConstructForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(403, 306);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ConstructForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MLP Network";
            this.panel1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTestPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSublayers)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.CheckBox checkBoxUseSOM;
        private System.Windows.Forms.NumericUpDown numericUpDownSublayers;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textBoxLayers;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBoxEpochs;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textBoxSOMEpochs;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox textBoxSOMDimensions;
        public System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonSelectData;
        public System.Windows.Forms.CheckBox checkBoxBias;
        public System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxSOMGrid;
        public System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownTestPercent;
        public System.Windows.Forms.Label label7;
    }
}

