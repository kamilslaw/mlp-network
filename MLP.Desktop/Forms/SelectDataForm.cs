﻿using MLP.Data;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MLP.Desktop.Forms
{
    public partial class SelectDataForm : Form
    {
        private readonly bool _forceClassColumnSelection;
        private int _state;

        public RawDataContext RawDataContext { get; private set; }

        public SelectDataForm() : this(null, false)
        {
        }

        public SelectDataForm(object[][] rawData, bool forceClassColumnSelection)
        {
            InitializeComponent();

            if (!DesignMode)
            {
                RawDataContext = new RawDataContext() { Data = rawData };
                _state = 1;
                _forceClassColumnSelection = forceClassColumnSelection;
                FillGrid();
            }
        }

        private void FillGrid()
        {
            for (int i = 0; i < RawDataContext.Data[0].Length; i++)
            {
                dataGridView.Columns.Add(i.ToString(), i.ToString());
                dataGridView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                dataGridView.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            foreach (var row in RawDataContext.Data.Take(22))
            {
                dataGridView.Rows.Add(row);
            }

            timerDeselectOnStart.Start();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            switch (_state)
            {
                case 1:
                    RawDataContext.ExcludedColumnsIndexes = new int[dataGridView.SelectedColumns.Count];
                    for (int i = 0; i < dataGridView.SelectedColumns.Count; i++)
                    {
                        RawDataContext.ExcludedColumnsIndexes[i] = dataGridView.SelectedColumns[i].Index;
                        dataGridView.SelectedColumns[i].DefaultCellStyle.BackColor = Color.Gainsboro;
                    }
                    labelInfo.Text = "Select class column";
                    dataGridView.MultiSelect = false;
                    break;

                case 2:
                    if (dataGridView.SelectedColumns.Count == 0 && _forceClassColumnSelection) return;
                    if (dataGridView.SelectedColumns.Count > 0) RawDataContext.ClassColumnIndex = dataGridView.SelectedColumns[0].Index;
                    for (int i = 0; i < dataGridView.SelectedColumns.Count; i++)
                        dataGridView.SelectedColumns[i].DefaultCellStyle.BackColor = Color.GreenYellow;
                    labelInfo.Text = "Select rows to exclude";
                    dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                    buttonNext.Text = "Train";
                    dataGridView.MultiSelect = true;
                    break;

                case 3:
                    RawDataContext.ExcludedRowsIndexes = new int[dataGridView.SelectedRows.Count];
                    for (int i = 0; i < dataGridView.SelectedRows.Count; i++)
                    {
                        RawDataContext.ExcludedRowsIndexes[i] = dataGridView.SelectedRows[i].Index;
                    }
                    DialogResult = DialogResult.Yes;
                    Close();
                    break;
            }

            dataGridView.ClearSelection();
            _state++;
        }

        private void timerDeselectOnStart_Tick(object sender, EventArgs e)
        {
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
            dataGridView.ClearSelection();
            timerDeselectOnStart.Stop();
        }
    }
}
