﻿namespace MLP.Core.Construction
{
    internal class Synapse
    {
        public INode Previous { get; set; }
        public INode Next { get; set; }
        public double W { get; set; }

        public Synapse(INode prev, INode next, double w)
        {
            Previous = prev;
            Next = next;
            W = w;
            Previous.NextSynapses.Add(this);
            Next.PreviousSynapses.Add(this);
        }
    }
}
