﻿using MLP.Data;
using MLP.Desktop.Forms;
using MLP.Desktop.Utils;
using System;
using System.Windows.Forms;

namespace AGDS.Desktop
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var filename = OpenFileHelper.GeDataFilenameFromDialog();
            if (string.IsNullOrEmpty(filename)) return;
            var rawData = DataReaderFactory.GetReader(filename).ReadRawData(filename);
            using (SelectDataForm selectDataForm = new SelectDataForm(rawData, forceClassColumnSelection: true))
            {
                if (selectDataForm.ShowDialog() == DialogResult.Yes)
                {
                    var networkData = new NetworkData(selectDataForm.RawDataContext);
                    Application.Run(new MainForm(networkData));
                }
            }
        }
    }
}
