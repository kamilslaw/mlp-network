﻿using System.Windows.Forms;

namespace MLP.Desktop.Utils
{
    public static class OpenFileHelper
    {
        public static string GeDataFilenameFromDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel/CSV files|*.xlsx;*.csv";
            openFileDialog.Title = "Select a data";

            return openFileDialog.ShowDialog() == DialogResult.OK ? openFileDialog.FileName : null;
        }
    }
}
