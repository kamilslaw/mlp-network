﻿using MLP.Data;
using System.Collections.Generic;
using System.Linq;

namespace MLP.Core.Models
{
    public class TrainingLog
    {
        public SOMColoring SOMColoring { get; set; }
        public NetworkData Data { get; set; }
        public NetworkSettings Settings { get; set; }
        public List<double> Errors { get; set; }
        public CheckingLog TrainDataChecking { get; set; }
        public CheckingLog TestDataChecking { get; set; }

        public double MinError { get { return Errors.Min(); } }
        public double MaxError { get { return Errors.Max(); } }
        public double FinalError { get { return Errors.Last(); } }

        public TrainingLog()
        {
            Errors = new List<double>();
        }
    }
}
