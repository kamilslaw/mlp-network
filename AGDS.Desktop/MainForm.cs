﻿using MLP.Data;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace AGDS.Desktop
{
    public partial class MainForm : Form
    {
        private AGDS _agds;

        public MainForm() : this(null)
        {
        }

        public MainForm(NetworkData networkData)
        {
            InitializeComponent();

            if (!DesignMode)
            {
                _agds = new AGDS(networkData);

                // DATA Tab
                dataGridViewData.Columns.Add("Name", "Name");
                for (int i = 1; i <= networkData.InputsCount; i++)
                    dataGridViewData.Columns.Add(i.ToString(), i.ToString());
                dataGridViewData.Columns.Add("Label", "Label");
                dataGridViewData.Columns[0].DefaultCellStyle.BackColor = dataGridViewData.Columns[networkData.InputsCount + 1].DefaultCellStyle.BackColor = Color.LightCyan;
                foreach (var column in dataGridViewData.Columns) (column as DataGridViewColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                foreach (var row in _agds.GetDataRows())
                    dataGridViewData.Rows.Add(row);

                // CLASSIFIER Tab
                dataGridViewClassifier.Columns.Add("Name", "Name");
                for (int i = 1; i <= networkData.InputsCount; i++)
                    dataGridViewClassifier.Columns.Add(i.ToString(), i.ToString());
                dataGridViewClassifier.Columns.Add("Expected label", "Expected label");
                dataGridViewClassifier.Columns.Add("Label", "Label");
                dataGridViewClassifier.Columns.Add("Matched", "Matched");
                dataGridViewClassifier.Columns[0].DefaultCellStyle.BackColor = dataGridViewClassifier.Columns[networkData.InputsCount + 1].DefaultCellStyle.BackColor = Color.LightCyan;
                foreach (var column in dataGridViewClassifier.Columns) (column as DataGridViewColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                int r = 0;
                foreach (var row in _agds.Rows)
                {
                    int predictedLabelIndex = _agds.Classify(row);
                    string label = networkData.Labels[predictedLabelIndex];
                    bool matched = predictedLabelIndex == row.LabelIndex;
                    var dataRow = row.ToDataRow(networkData.Labels, _agds.Nodes).ToList();
                    dataRow.Add(label);
                    dataRow.Add(matched ? "Y" : "N");
                    dataGridViewClassifier.Rows.Add(dataRow.ToArray());
                    dataGridViewClassifier[networkData.InputsCount + 2, r].Style.BackColor = matched ? Color.GreenYellow : Color.Tomato;
                    r++;
                }

                // COMPARE tab
                dataGridViewCompareSelect.Columns.Add("Row", "Row");
                dataGridViewCompareSelect.Columns[0].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataGridViewCompareSelect.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                dataGridViewCompareSelect.Columns[0].Width = 60;

                foreach (var row in _agds.Rows)
                    dataGridViewCompareSelect.Rows.Add(row.Name);


                dataGridViewCompare.Columns.Add("Name", "Name");
                for (int i = 1; i <= networkData.InputsCount; i++)
                    dataGridViewCompare.Columns.Add(i.ToString(), i.ToString());
                dataGridViewCompare.Columns.Add("Label", "Label");
                dataGridViewCompare.Columns.Add("Similarity", "Similarity");
                dataGridViewCompare.Columns[0].DefaultCellStyle.BackColor = Color.LightCyan;
                foreach (var column in dataGridViewCompare.Columns) (column as DataGridViewColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void dataGridViewCompareSelect_SelectionChanged(object sender, EventArgs e)
        {
            var row = _agds.Rows[dataGridViewCompareSelect.SelectedRows[0].Index];
            var results = _agds.GetSimilarity(row);

            dataGridViewCompare.Rows.Clear();
            int r = 0;
            foreach (var result in results)
            {
                var dataRow = result.row.ToDataRow(_agds.Data.Labels, _agds.Nodes).ToList();
                dataRow.Add(result.factor.ToString("N2"));
                dataGridViewCompare.Rows.Add(dataRow.ToArray());
                dataGridViewCompare[_agds.Data.InputsCount + 2, r].Style.BackColor = Color.FromArgb((int)(Math.Max(0, 1 - result.factor) * 255), 255, (int)(Math.Max(0, 1 - result.factor) * 255));
                r++;
            }
            dataGridViewCompare.ClearSelection();
        }
    }
}
