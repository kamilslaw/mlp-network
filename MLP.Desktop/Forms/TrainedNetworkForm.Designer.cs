﻿namespace MLP.Desktop.Forms
{
    partial class TrainedNetworkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrainedNetworkForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageError = new System.Windows.Forms.TabPage();
            this.chartError = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPageSOM = new System.Windows.Forms.TabPage();
            this.checkedListBoxSOMClasses = new System.Windows.Forms.CheckedListBox();
            this.panelSOMDraw = new System.Windows.Forms.Panel();
            this.tabPageTrainingResults = new System.Windows.Forms.TabPage();
            this.dataGridViewTrainingData = new System.Windows.Forms.DataGridView();
            this.dataGridViewTest = new System.Windows.Forms.DataGridView();
            this.tabPageResultTest = new System.Windows.Forms.TabPage();
            this.tabControl.SuspendLayout();
            this.tabPageError.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartError)).BeginInit();
            this.tabPageSOM.SuspendLayout();
            this.tabPageTrainingResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrainingData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTest)).BeginInit();
            this.tabPageResultTest.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(51)))));
            this.panel1.Location = new System.Drawing.Point(0, 628);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(892, 60);
            this.panel1.TabIndex = 14;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPageError);
            this.tabControl.Controls.Add(this.tabPageSOM);
            this.tabControl.Controls.Add(this.tabPageTrainingResults);
            this.tabControl.Controls.Add(this.tabPageResultTest);
            this.tabControl.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabControl.Location = new System.Drawing.Point(1, 0);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(891, 628);
            this.tabControl.TabIndex = 15;
            this.tabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl_Selected);
            // 
            // tabPageError
            // 
            this.tabPageError.Controls.Add(this.chartError);
            this.tabPageError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(40)))), ((int)(((byte)(38)))));
            this.tabPageError.Location = new System.Drawing.Point(4, 30);
            this.tabPageError.Name = "tabPageError";
            this.tabPageError.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageError.Size = new System.Drawing.Size(883, 594);
            this.tabPageError.TabIndex = 2;
            this.tabPageError.Text = "Error";
            this.tabPageError.UseVisualStyleBackColor = true;
            // 
            // chartError
            // 
            this.chartError.BorderlineWidth = 3;
            chartArea1.Name = "ChartArea1";
            this.chartError.ChartAreas.Add(chartArea1);
            this.chartError.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartError.Location = new System.Drawing.Point(3, 3);
            this.chartError.Name = "chartError";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Name = "Series1";
            this.chartError.Series.Add(series1);
            this.chartError.Size = new System.Drawing.Size(877, 588);
            this.chartError.TabIndex = 1;
            this.chartError.Text = "chart1";
            this.chartError.MouseClick += new System.Windows.Forms.MouseEventHandler(this.chartError_MouseClick);
            // 
            // tabPageSOM
            // 
            this.tabPageSOM.Controls.Add(this.checkedListBoxSOMClasses);
            this.tabPageSOM.Controls.Add(this.panelSOMDraw);
            this.tabPageSOM.Location = new System.Drawing.Point(4, 30);
            this.tabPageSOM.Name = "tabPageSOM";
            this.tabPageSOM.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSOM.Size = new System.Drawing.Size(712, 480);
            this.tabPageSOM.TabIndex = 3;
            this.tabPageSOM.Text = "SOM";
            this.tabPageSOM.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxSOMClasses
            // 
            this.checkedListBoxSOMClasses.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.checkedListBoxSOMClasses.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBoxSOMClasses.CheckOnClick = true;
            this.checkedListBoxSOMClasses.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkedListBoxSOMClasses.FormattingEnabled = true;
            this.checkedListBoxSOMClasses.Items.AddRange(new object[] {
            "Class 1",
            "Class 2",
            "Class 3",
            "Class 4"});
            this.checkedListBoxSOMClasses.Location = new System.Drawing.Point(6, 1);
            this.checkedListBoxSOMClasses.Name = "checkedListBoxSOMClasses";
            this.checkedListBoxSOMClasses.Size = new System.Drawing.Size(190, 476);
            this.checkedListBoxSOMClasses.TabIndex = 0;
            this.checkedListBoxSOMClasses.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBoxSOMClasses_ItemCheck);
            this.checkedListBoxSOMClasses.SelectedIndexChanged += new System.EventHandler(this.checkedListBoxSOMClasses_SelectedIndexChanged);
            // 
            // panelSOMDraw
            // 
            this.panelSOMDraw.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelSOMDraw.Location = new System.Drawing.Point(199, 0);
            this.panelSOMDraw.Name = "panelSOMDraw";
            this.panelSOMDraw.Size = new System.Drawing.Size(513, 480);
            this.panelSOMDraw.TabIndex = 2;
            this.panelSOMDraw.Resize += new System.EventHandler(this.panelSOMDraw_Resize);
            // 
            // tabPageTrainingResults
            // 
            this.tabPageTrainingResults.Controls.Add(this.dataGridViewTrainingData);
            this.tabPageTrainingResults.Location = new System.Drawing.Point(4, 30);
            this.tabPageTrainingResults.Name = "tabPageTrainingResults";
            this.tabPageTrainingResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTrainingResults.Size = new System.Drawing.Size(712, 480);
            this.tabPageTrainingResults.TabIndex = 4;
            this.tabPageTrainingResults.Text = "Results for training data";
            this.tabPageTrainingResults.UseVisualStyleBackColor = true;
            // 
            // dataGridViewTrainingData
            // 
            this.dataGridViewTrainingData.AllowUserToAddRows = false;
            this.dataGridViewTrainingData.AllowUserToDeleteRows = false;
            this.dataGridViewTrainingData.AllowUserToResizeColumns = false;
            this.dataGridViewTrainingData.AllowUserToResizeRows = false;
            this.dataGridViewTrainingData.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewTrainingData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTrainingData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTrainingData.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTrainingData.Name = "dataGridViewTrainingData";
            this.dataGridViewTrainingData.ReadOnly = true;
            this.dataGridViewTrainingData.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataGridViewTrainingData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewTrainingData.Size = new System.Drawing.Size(706, 474);
            this.dataGridViewTrainingData.TabIndex = 1;
            // 
            // dataGridViewTest
            // 
            this.dataGridViewTest.AllowUserToAddRows = false;
            this.dataGridViewTest.AllowUserToDeleteRows = false;
            this.dataGridViewTest.AllowUserToResizeColumns = false;
            this.dataGridViewTest.AllowUserToResizeRows = false;
            this.dataGridViewTest.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewTest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewTest.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewTest.Name = "dataGridViewTest";
            this.dataGridViewTest.ReadOnly = true;
            this.dataGridViewTest.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataGridViewTest.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewTest.Size = new System.Drawing.Size(706, 474);
            this.dataGridViewTest.TabIndex = 2;
            // 
            // tabPageResultTest
            // 
            this.tabPageResultTest.Controls.Add(this.dataGridViewTest);
            this.tabPageResultTest.Location = new System.Drawing.Point(4, 30);
            this.tabPageResultTest.Name = "tabPageResultTest";
            this.tabPageResultTest.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageResultTest.Size = new System.Drawing.Size(712, 480);
            this.tabPageResultTest.TabIndex = 5;
            this.tabPageResultTest.Text = "Results for test data";
            this.tabPageResultTest.UseVisualStyleBackColor = true;
            // 
            // TrainedNetworkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(892, 688);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TrainedNetworkForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MLP Network";
            this.tabControl.ResumeLayout(false);
            this.tabPageError.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartError)).EndInit();
            this.tabPageSOM.ResumeLayout(false);
            this.tabPageTrainingResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrainingData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTest)).EndInit();
            this.tabPageResultTest.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageError;
        private System.Windows.Forms.TabPage tabPageSOM;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartError;
        private System.Windows.Forms.TabPage tabPageTrainingResults;
        private System.Windows.Forms.CheckedListBox checkedListBoxSOMClasses;
        private System.Windows.Forms.Panel panelSOMDraw;
        private System.Windows.Forms.DataGridView dataGridViewTrainingData;
        private System.Windows.Forms.TabPage tabPageResultTest;
        private System.Windows.Forms.DataGridView dataGridViewTest;
    }
}