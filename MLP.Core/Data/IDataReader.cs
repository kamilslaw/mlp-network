﻿namespace MLP.Data
{
    public interface IDataReader
    {
        object[][] ReadRawData(string filename);
    }
}
