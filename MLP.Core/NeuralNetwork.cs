﻿using MLP.Core.Construction;
using MLP.Core.Helpers;
using MLP.Core.Models;
using MLP.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MLP.Core
{
    public class NeuralNetwork
    {
        private readonly NetworkData _data;
        private readonly List<DataRow> _testData;
        private readonly DataRow[] _rawData;
        private readonly NetworkSettings _settings;
        private readonly NetworkStructure _network;
        private readonly SOMStructure _som;

        public NeuralNetwork(NetworkData data, NetworkSettings settings)
        {
            if (settings.ExtractToTest)
            {
                _testData = new List<DataRow>();
                List<int> usedIndexes = new List<int>();
                int elementsToExtract = data.Values.Length * settings.PercentToTest / 100 / data.Labels.Length;
                for (int i = 0; i < data.InputsCount; i++)
                {
                    int[] indexesToDiscover = Enumerable.Range(0, data.Values.Length).ToArray();
                    indexesToDiscover.Shuffle();


                    for (int j = 0; j < data.Values.Length; j++)
                    {
                        if (usedIndexes.Count == elementsToExtract) break;
                        if (data.Values[indexesToDiscover[j]].LabelIndex == i)
                        {
                            _testData.Add(data.Values[indexesToDiscover[j]]);
                            usedIndexes.Add(indexesToDiscover[j]);
                        }
                    }                   
                    data.Values = data.Values.Where((_, index) => !usedIndexes.Contains(index)).ToArray();
                    usedIndexes.Clear();
                }
            }
            _settings = settings;
            _rawData = data.Values.Select(d => d.Clone()).ToArray();
            _data = PrePrepareData(data);
            if (settings.UseSom) _som = new SOMStructure(_data, settings);
            _network = new NetworkStructure(_data, settings, _som);
        }

        #region Public Methods

        public TrainingLog Train(IProgress<int> progressHandler)
        {
            var result = new TrainingLog() { Settings = _settings, Data = _data };

            var updateProgressTreshold = _settings.EpochsTotal / 100;
            var cyclesToUpdate = 0;
            int step = 0;
            // SOM learning
            if (_settings.UseSom)
            {
                for (int c = 0; c < _settings.SOMEpochs; c++)
                {
                    _som.Train(c);

                    if (cyclesToUpdate % updateProgressTreshold == 0) progressHandler.Report(step * 100 / _settings.EpochsTotal);
                    cyclesToUpdate++;
                    step++;
                }
                result.SOMColoring = _som.GenarateColors();
            }
            // Network learing
            for (int s = 0; s < _settings.SubLayersCount; s++)
            {
                if (s > 0) _network.AddSublayer(s);
                for (int c = 0; c < _settings.Epochs; c++)
                {
                    var error = _network.Train();
                    result.Errors.Add(error / (_data.OutputsCount * _data.Values.Length));

                    if (cyclesToUpdate % updateProgressTreshold == 0) progressHandler.Report(step * 100 / _settings.EpochsTotal);
                    cyclesToUpdate++;
                    step++;
                }
            }
            // Checking for train data
            result.TrainDataChecking = _network.Check(_rawData);
            if (_settings.ExtractToTest) result.TestDataChecking = _network.Check(_testData.ToArray());

            progressHandler.Report(100);
            return result;
        }

        #endregion

        #region Private Methods

        private static NetworkData PrePrepareData(NetworkData data)
        {
            data.Values.Shuffle();
            data.Values.Normalize();
            return data;
        }

        #endregion
    }
}
