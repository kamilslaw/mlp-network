﻿using MLP.Core.Models;
using MLP.Data;
using System;

namespace MLP.Core.Construction
{
    internal class BaseStructure
    {
        protected static readonly Random _random = new Random();

        protected readonly NetworkData _data;
        protected readonly NetworkSettings _settings;

        public BaseStructure(NetworkData data, NetworkSettings settings)
        {
            _data = data;
            _settings = settings;
        }
    }
}
