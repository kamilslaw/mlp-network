﻿using MLP.Data;
using System.Collections.Generic;

namespace MLP.Core.Models
{
    public class CheckingLog
    {
        public List<CheckingLogPosition> Values { get; }

        public CheckingLog()
        {
            Values = new List<CheckingLogPosition>();
        }

        internal void AddPosition(DataRow dataRow, string label, double[] values, double error, bool? isCorrect)
        {
            Values.Add(new CheckingLogPosition(label, values, dataRow, error, isCorrect));
        }
    }

    public class CheckingLogPosition
    {
        public CheckingLogPosition()
        { }

        public CheckingLogPosition(string label, double[] values, DataRow dataRow, double error, bool? isCorrect)
        {
            Label = label;
            Values = values;
            DataRow = dataRow;
            Error = error;
            IsCorrect = isCorrect;
        }

        public string Label { get; set; }
        public double[] Values { get; set; }
        public DataRow DataRow { get; set; }
        public double Error { get; set; }
        public bool? IsCorrect { get; set; }
    }
}
