﻿using MLP.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MLP.Core.Models
{
    public class SOMColoring
    {
        private Array _nodesLabels;

        public SOMColoring(int[] dimensions)
        {
            _nodesLabels = Array.CreateInstance(typeof(List<int>), dimensions);
            _nodesLabels.ForEachNode<List<int>>((_, coordinates) => _nodesLabels.SetValue(new List<int>(), coordinates));
        }

        public Color GetColorForNode(int[] labelsIndexes, int[] index)
        {
            if (labelsIndexes == null || !labelsIndexes.Any() || labelsIndexes.Length > 3)
            {
                throw new ArgumentOutOfRangeException("To get color, you need to pass from 1 to 3 labels");
            }

            var labels = _nodesLabels.GetValue(index) as List<int>;
            int totalLabelCount = labels.Count(l => labelsIndexes.Contains(l));
            if (totalLabelCount == 0) return Color.Transparent;

            var r = (int)(255.0 * labels.Count(l => l == labelsIndexes[0]) / totalLabelCount);
            var g = labelsIndexes.Length > 1 ? (int)(255.0 * labels.Count(l => l == labelsIndexes[1]) / totalLabelCount) : 0;
            var b = labelsIndexes.Length > 2 ? (int)(255.0 * labels.Count(l => l == labelsIndexes[2]) / totalLabelCount) : 0;
            return Color.FromArgb(r, g, b);
        }

        public Tuple<int[], Color>[] GetColors(int[] labelsIndexes)
        {
            if (labelsIndexes == null || !labelsIndexes.Any() || labelsIndexes.Length > 3)
            {
                throw new ArgumentOutOfRangeException("To get color, you need to pass from 1 to 3 labels");
            }

            return _nodesLabels.SelectEachNode<List<int>, Tuple<int[], Color>>((node, coordinates) => Tuple.Create(coordinates, GetColorForNode(labelsIndexes, coordinates))).ToArray();
        }

        internal void AddElement(int labelIndex, int[] referenceIndex)
        {
            var nodesList = _nodesLabels.GetValue(referenceIndex) as List<int>;
            nodesList.Add(labelIndex);
        }
    }
}
