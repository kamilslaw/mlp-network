﻿using System;
using System.IO;
using System.Linq;

namespace MLP.Data
{
    public class CsvDataReader : IDataReader
    {
        public object[][] ReadRawData(string filename)
        {
            string[] lines = File.ReadAllLines(filename);
            return lines.Select(l => l.Split(new char[] { ';' }, StringSplitOptions.None)).ToArray();
        }
    }
}
