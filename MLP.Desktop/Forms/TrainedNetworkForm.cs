﻿using MLP.Core.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MLP.Desktop.Forms
{
    public partial class TrainedNetworkForm : Form
    {
        private TrainingLog _result;

        private Tuple<int[], Color>[] _somColors;
        private int _somSingleWidth, _somSingleHeight;

        public TrainedNetworkForm() : this(null)
        {
        }

        public TrainedNetworkForm(TrainingLog trainingResult)
        {
            InitializeComponent();

            if (!DesignMode)
            {
                _result = trainingResult;

                // SOM
                if (!ShowSOM())
                {
                    tabControl.TabPages.Remove(tabPageSOM);
                }
                else
                {
                    checkedListBoxSOMClasses.Items.Clear();
                    checkedListBoxSOMClasses.Items.AddRange(_result.Data.Labels);
                    //Enumerable.Range(0, Math.Min(3, _result.Data.Labels.Length)).ToList().ForEach(i => checkedListBoxSOMClasses.SetItemCheckState(i, CheckState.Checked));
                }
                // ERROR
                chartError.Series[0].ChartType = SeriesChartType.Line;
                chartError.ChartAreas[0].AxisX.ScaleView.Zoom(1, trainingResult.Errors.Count);
                chartError.ChartAreas[0].AxisY.ScaleView.Zoom(0, trainingResult.MaxError);
                chartError.ChartAreas[0].AxisX.LabelStyle.Format = "{0:0}";
                chartError.ChartAreas[0].AxisY.LabelStyle.Format = "{0:0.000000}";
                chartError.ChartAreas[0].AxisX.Title = "Epoch";
                chartError.ChartAreas[0].AxisY.Title = "Error rate";
                chartError.ChartAreas[0].AxisX.ScrollBar.Enabled = false;
                chartError.ChartAreas[0].AxisY.ScrollBar.Enabled = false;
                for (int i = 0; i < trainingResult.Errors.Count; i++)
                {
                    chartError.Series[0].Points.AddXY(i, trainingResult.Errors[i]);
                }
                // TRAINING DATA RESULTS
                FillGrid(trainingResult, trainingResult.TrainDataChecking, dataGridViewTrainingData);
                // TEST DATA RESULTS
                if (!trainingResult.Settings.ExtractToTest)
                {
                    tabControl.TabPages.Remove(tabPageResultTest);
                }
                else
                {
                    FillGrid(trainingResult, trainingResult.TestDataChecking, dataGridViewTest);
                }
            }
        }

        private bool ShowSOM()
        {
            return _result.Settings.UseSom && _result.Settings.SOMDimensions.Length <= 3;
        }

        private void checkedListBoxSOMClasses_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox items = (CheckedListBox)sender;
            if (items.CheckedItems.Count > 3)
            {
                e.NewValue = CheckState.Unchecked;
            }
            else if (items.CheckedItems.Count == 1 && e.NewValue == CheckState.Unchecked)
            {
                e.NewValue = CheckState.Checked;
            }
        }

        private void checkedListBoxSOMClasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateSOMDiagram();
        }

        private void panelSOMDraw_Resize(object sender, EventArgs e)
        {
            RedrawSOMDiagram();
        }

        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            if (tabControl.SelectedTab == tabPageSOM)
            {
                UpdateSOMDiagram();
            }
        }

        private void chartError_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                chartError.ChartAreas[0].AxisY.ScaleView.Zoom(0, chartError.ChartAreas[0].AxisY.ScaleView.Size / 2);
            }
            else if (e.Button == MouseButtons.Right)
            {
                chartError.ChartAreas[0].AxisY.ScaleView.Zoom(0, chartError.ChartAreas[0].AxisY.ScaleView.Size * 2);
            }
        }

        private void UpdateSOMDiagram()
        {
            if (ShowSOM())
            {
                var selectedIndexes = checkedListBoxSOMClasses.CheckedIndices.Cast<int>().ToArray();
                if (selectedIndexes.Any())
                {
                    _somColors = _result.SOMColoring.GetColors(selectedIndexes);
                }
                RedrawSOMDiagram();
            }
        }

        private void RedrawSOMDiagram()
        {
            if (ShowSOM() && _somColors != null)
            {
                var g = panelSOMDraw.CreateGraphics();
                g.Clear(Color.White);

                switch (_result.Settings.SOMDimensions.Length)
                {
                    case 1:
                        foreach (var tuple in _somColors)
                        {
                            int xElements = _result.Settings.SOMDimensions[0];
                            int width = panelSOMDraw.Bounds.Width / xElements;
                            int height = panelSOMDraw.Bounds.Height;
                            var rectangle = new Rectangle(width * tuple.Item1[0], 0, width - 1, height);
                            g.FillRectangle(new SolidBrush(tuple.Item2), rectangle);
                        }
                        break;

                    case 2:
                        foreach (var tuple in _somColors)
                        {
                            int xElements = _result.Settings.SOMDimensions[0];
                            int yElements = _result.Settings.SOMDimensions[1];
                            int width = panelSOMDraw.Bounds.Width / xElements;
                            int height = panelSOMDraw.Bounds.Height / yElements;
                            var rectangle = new Rectangle(width * tuple.Item1[0], height * tuple.Item1[1], width - 1, height - 1);
                            g.FillRectangle(new SolidBrush(tuple.Item2), rectangle);
                        }
                        break;

                    case 3:
                        int xCount = _result.Settings.SOMDimensions[0];
                        int yCount = _result.Settings.SOMDimensions[1];
                        int zCount = _result.Settings.SOMDimensions[2];
                        int xMax = _result.Settings.SOMDimensions[0] - 1;
                        int yMax = _result.Settings.SOMDimensions[1] - 1;
                        int zMax = _result.Settings.SOMDimensions[2] - 1;
                        _somSingleWidth = panelSOMDraw.Bounds.Width / (zCount + zCount + xCount + xCount);
                        _somSingleHeight = panelSOMDraw.Bounds.Height / (zCount + yCount + yCount);

                        WriteCubeWall(g, _somColors.Where(c => c.Item1[2] == 0), false, false, 0, 1, zCount * _somSingleWidth, zCount * _somSingleHeight);
                        WriteCubeWall(g, _somColors.Where(c => c.Item1[0] == xMax), false, false, 2, 1, (zCount + xCount) * _somSingleWidth, zCount * _somSingleHeight);
                        WriteCubeWall(g, _somColors.Where(c => c.Item1[1] == 0), false, true, 0, 2, zCount * _somSingleWidth, 0);
                        WriteCubeWall(g, _somColors.Where(c => c.Item1[1] == yMax), false, false, 0, 2, zCount * _somSingleWidth, (zCount + yCount) * _somSingleHeight);
                        WriteCubeWall(g, _somColors.Where(c => c.Item1[2] == zMax), true, false, 0, 1, (zCount + xCount + zCount) * _somSingleWidth, zCount * _somSingleHeight);
                        WriteCubeWall(g, _somColors.Where(c => c.Item1[0] == 0), true, false, 2, 1, 0, zCount * _somSingleHeight);
                        break;
                }
            }
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {

        }

        private void WriteCubeWall(Graphics g, IEnumerable<Tuple<int[], Color>> tuples, bool inverseF, bool inverseS, int firstAxeIndex, int secondAxeIndex, int startWidth, int startHeight)
        {
            int fElements = _result.Settings.SOMDimensions[firstAxeIndex];
            int sElements = _result.Settings.SOMDimensions[secondAxeIndex];
            foreach (var tuple in tuples)
            {
                var rectangle = new Rectangle(1 + startWidth + _somSingleWidth * (inverseF ? -1 + fElements - tuple.Item1[firstAxeIndex] : tuple.Item1[firstAxeIndex]),
                                              1 + startHeight + _somSingleHeight * (inverseS ? -1 + sElements - tuple.Item1[secondAxeIndex] : tuple.Item1[secondAxeIndex]),
                                              _somSingleWidth - 2,
                                              _somSingleHeight - 2);

                if (tuple.Item2 != Color.Transparent)
                {
                    g.FillRectangle(new SolidBrush(tuple.Item2), rectangle);
                }
                g.DrawRectangle(new Pen(Color.Black, 1), rectangle);
            }
        }

        private void FillGrid(TrainingLog trainingResult, CheckingLog checkingResult, DataGridView grid)
        {
            int attributesCount = checkingResult.Values[0].DataRow.Count;
            int outputsCount = checkingResult.Values[0].Values.Length;
            grid.Columns.Add("No", "No");
            for (int i = 0; i < attributesCount; i++)
                grid.Columns.Add(i.ToString(), i.ToString());
            for (int i = 0; i < outputsCount; i++)
                grid.Columns.Add("O" + i, "O" + i);
            grid.Columns.Add("Label", "Label");
            grid.Columns.Add("Expected", "Expected");
            grid.Columns.Add("Error", "Error");
            grid.Columns.Add("OK", "OK");
            foreach (var column in grid.Columns) (column as DataGridViewColumn).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grid.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            grid.Columns[0].DefaultCellStyle.BackColor = Color.MintCream;
            for (int i = 0; i < outputsCount; i++)
            {
                grid.Columns[i + attributesCount + 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                grid.Columns[i + attributesCount + 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
            }
            grid.Columns[attributesCount + outputsCount + 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            grid.Columns[attributesCount + outputsCount + 2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            grid.Columns[attributesCount + outputsCount + 3].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grid.Columns[attributesCount + outputsCount + 4].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            grid.Columns[attributesCount + outputsCount + 1].DefaultCellStyle.BackColor = Color.GreenYellow;
            grid.Columns[attributesCount + outputsCount + 2].DefaultCellStyle.BackColor = Color.LightCyan;
            int r = 0;
            foreach (var row in checkingResult.Values)
            {
                object[] data = new object[attributesCount + outputsCount + 5];
                data[0] = r + 1;
                for (int i = 0; i < attributesCount; i++) data[i + 1] = row.DataRow[i];
                for (int i = 0; i < outputsCount; i++) data[i + attributesCount + 1] = $"{row.Values[i]:0.00}";
                data[attributesCount + outputsCount + 1] = row.Label;
                data[attributesCount + outputsCount + 2] = trainingResult.Data.Labels[row.DataRow.LabelIndex];
                data[attributesCount + outputsCount + 3] = $"{row.Error:0.00}";
                data[attributesCount + outputsCount + 4] = row.IsCorrect.Value ? "OK" : "X";
                grid.Rows.Add(data);
                if (!row.IsCorrect.Value)
                    grid[attributesCount + outputsCount + 1, r].Style.BackColor = Color.Tomato;
                grid[attributesCount + outputsCount + 3, r].Style.BackColor = Color.FromArgb(255, (int)(Math.Max(0, 1 - row.Error) * 255), (int)(Math.Max(0, 1 - row.Error) * 255));
                r++;
            }
        }
    }
}
