﻿namespace AGDS.Desktop
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabData = new System.Windows.Forms.TabPage();
            this.dataGridViewData = new System.Windows.Forms.DataGridView();
            this.tabClassifier = new System.Windows.Forms.TabPage();
            this.dataGridViewClassifier = new System.Windows.Forms.DataGridView();
            this.tabPageCompare = new System.Windows.Forms.TabPage();
            this.dataGridViewCompare = new System.Windows.Forms.DataGridView();
            this.dataGridViewCompareSelect = new System.Windows.Forms.DataGridView();
            this.tabControl.SuspendLayout();
            this.tabData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).BeginInit();
            this.tabClassifier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClassifier)).BeginInit();
            this.tabPageCompare.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCompare)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCompareSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabData);
            this.tabControl.Controls.Add(this.tabClassifier);
            this.tabControl.Controls.Add(this.tabPageCompare);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(767, 621);
            this.tabControl.TabIndex = 0;
            // 
            // tabData
            // 
            this.tabData.Controls.Add(this.dataGridViewData);
            this.tabData.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tabData.Location = new System.Drawing.Point(4, 30);
            this.tabData.Name = "tabData";
            this.tabData.Padding = new System.Windows.Forms.Padding(3);
            this.tabData.Size = new System.Drawing.Size(759, 587);
            this.tabData.TabIndex = 0;
            this.tabData.Text = "Data";
            this.tabData.UseVisualStyleBackColor = true;
            // 
            // dataGridViewData
            // 
            this.dataGridViewData.AllowUserToAddRows = false;
            this.dataGridViewData.AllowUserToDeleteRows = false;
            this.dataGridViewData.AllowUserToResizeColumns = false;
            this.dataGridViewData.AllowUserToResizeRows = false;
            this.dataGridViewData.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewData.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewData.Name = "dataGridViewData";
            this.dataGridViewData.ReadOnly = true;
            this.dataGridViewData.RowHeadersVisible = false;
            this.dataGridViewData.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataGridViewData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewData.Size = new System.Drawing.Size(753, 581);
            this.dataGridViewData.TabIndex = 2;
            // 
            // tabClassifier
            // 
            this.tabClassifier.Controls.Add(this.dataGridViewClassifier);
            this.tabClassifier.Location = new System.Drawing.Point(4, 30);
            this.tabClassifier.Name = "tabClassifier";
            this.tabClassifier.Padding = new System.Windows.Forms.Padding(3);
            this.tabClassifier.Size = new System.Drawing.Size(759, 587);
            this.tabClassifier.TabIndex = 1;
            this.tabClassifier.Text = "Classifier";
            this.tabClassifier.UseVisualStyleBackColor = true;
            // 
            // dataGridViewClassifier
            // 
            this.dataGridViewClassifier.AllowUserToAddRows = false;
            this.dataGridViewClassifier.AllowUserToDeleteRows = false;
            this.dataGridViewClassifier.AllowUserToResizeColumns = false;
            this.dataGridViewClassifier.AllowUserToResizeRows = false;
            this.dataGridViewClassifier.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewClassifier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewClassifier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewClassifier.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewClassifier.Name = "dataGridViewClassifier";
            this.dataGridViewClassifier.ReadOnly = true;
            this.dataGridViewClassifier.RowHeadersVisible = false;
            this.dataGridViewClassifier.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataGridViewClassifier.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewClassifier.Size = new System.Drawing.Size(753, 581);
            this.dataGridViewClassifier.TabIndex = 3;
            // 
            // tabPageCompare
            // 
            this.tabPageCompare.Controls.Add(this.dataGridViewCompare);
            this.tabPageCompare.Controls.Add(this.dataGridViewCompareSelect);
            this.tabPageCompare.Location = new System.Drawing.Point(4, 30);
            this.tabPageCompare.Name = "tabPageCompare";
            this.tabPageCompare.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCompare.Size = new System.Drawing.Size(759, 587);
            this.tabPageCompare.TabIndex = 2;
            this.tabPageCompare.Text = "Compare";
            this.tabPageCompare.UseVisualStyleBackColor = true;
            // 
            // dataGridViewCompare
            // 
            this.dataGridViewCompare.AllowUserToAddRows = false;
            this.dataGridViewCompare.AllowUserToDeleteRows = false;
            this.dataGridViewCompare.AllowUserToResizeColumns = false;
            this.dataGridViewCompare.AllowUserToResizeRows = false;
            this.dataGridViewCompare.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewCompare.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewCompare.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCompare.Location = new System.Drawing.Point(81, 0);
            this.dataGridViewCompare.Name = "dataGridViewCompare";
            this.dataGridViewCompare.ReadOnly = true;
            this.dataGridViewCompare.RowHeadersVisible = false;
            this.dataGridViewCompare.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataGridViewCompare.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCompare.Size = new System.Drawing.Size(678, 587);
            this.dataGridViewCompare.TabIndex = 5;
            // 
            // dataGridViewCompareSelect
            // 
            this.dataGridViewCompareSelect.AllowUserToAddRows = false;
            this.dataGridViewCompareSelect.AllowUserToDeleteRows = false;
            this.dataGridViewCompareSelect.AllowUserToResizeColumns = false;
            this.dataGridViewCompareSelect.AllowUserToResizeRows = false;
            this.dataGridViewCompareSelect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dataGridViewCompareSelect.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewCompareSelect.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCompareSelect.Location = new System.Drawing.Point(2, 0);
            this.dataGridViewCompareSelect.MultiSelect = false;
            this.dataGridViewCompareSelect.Name = "dataGridViewCompareSelect";
            this.dataGridViewCompareSelect.ReadOnly = true;
            this.dataGridViewCompareSelect.RowHeadersVisible = false;
            this.dataGridViewCompareSelect.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dataGridViewCompareSelect.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCompareSelect.Size = new System.Drawing.Size(80, 587);
            this.dataGridViewCompareSelect.TabIndex = 4;
            this.dataGridViewCompareSelect.SelectionChanged += new System.EventHandler(this.dataGridViewCompareSelect_SelectionChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(767, 621);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AGDS";
            this.tabControl.ResumeLayout(false);
            this.tabData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewData)).EndInit();
            this.tabClassifier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewClassifier)).EndInit();
            this.tabPageCompare.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCompare)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCompareSelect)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabData;
        private System.Windows.Forms.TabPage tabClassifier;
        private System.Windows.Forms.DataGridView dataGridViewData;
        private System.Windows.Forms.DataGridView dataGridViewClassifier;
        private System.Windows.Forms.TabPage tabPageCompare;
        private System.Windows.Forms.DataGridView dataGridViewCompareSelect;
        private System.Windows.Forms.DataGridView dataGridViewCompare;
    }
}