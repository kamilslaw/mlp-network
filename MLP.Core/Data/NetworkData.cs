﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MLP.Data
{
    public class NetworkData
    {
        public string[] Labels { get; set; }
        public DataRow[] Values { get; set; }

        public int InputsCount { get { return Values[0].Count; } }
        public int OutputsCount { get { return Labels.Length; } }

        public NetworkData(RawDataContext context)
        {
            Values = new DataRow[context.RowsCount];
            if (context.ClassColumnIndex >= 0)
            {
                var labelsList = new List<string>();
                for (int i = 0; i < context.RowsCount; i++)
                    if (!context.ExcludedRowsIndexes.Contains(i))
                        labelsList.Add(context.Data[i][context.ClassColumnIndex].ToString());
                Labels = labelsList.Distinct().ToArray();
            }

            int k = 0;
            for (int i = 0; i < context.Data.Length; i++)
                if (!context.ExcludedRowsIndexes.Contains(i))
                {
                    var labelIndex = Array.IndexOf(Labels, context.Data[i][context.ClassColumnIndex]);
                    var values = context.Data[i].Where((_, j) => j != context.ClassColumnIndex && !context.ExcludedColumnsIndexes.Contains(j))
                                                .Select(cell => Convert.ToDouble(cell, CultureInfo.InvariantCulture))
                                                .ToArray();
                    Values[k] = DataRow.Create(labelIndex, values);
                    k++;
                }
        }
    }

    public class DataRow : List<double>
    {
        public int LabelIndex { get; set; }

        public static DataRow Create(int labelIndex, params double[] values)
        {
            DataRow result = new DataRow() { LabelIndex = labelIndex };
            result.AddRange(values);
            return result;
        }

        public static DataRow Create(params double[] values)
        {
            return Create(-1, values);
        }

        public DataRow Clone() => Create(LabelIndex, ToArray());
    }
}